import CartIcon from '../Cart/CartIcon';
import classses from './HeaderCartButton.module.css';
import CartContext from '../../store/CartContext';
import {useContext} from 'react';

function HeaderCartButton(props){
    const cartCtx = useContext(CartContext);
    const numberOfCartItems = cartCtx.items.reduce( (currCount , item) => {
      currCount += item.amount;
      return currCount
    },0);
    // console.log(cartCtx.items)
    // console.log("abhi" + numberOfCartItems)
    return (
      <button className={classses.button} onClick={props.showCartHandler} >
        <span className={classses.icon}>
          <CartIcon />
        </span>
        <span>You Cart </span>
        <span className={classses.badge}>{numberOfCartItems}</span>
      </button> 
    )
}

export default HeaderCartButton;