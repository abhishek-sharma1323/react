
import mealsImage from '../../assests/meals.jpg';
import classes from './Header.module.css';
import HeaderCartButton from './HeaderCartButton';


function Header(props){
  return (
    <>
    <header className={classes.header}>
      <h1>FriendsHotel</h1>
      <HeaderCartButton showCartHandler={props.showCartHandler} />
    </header>
    <div className={classes['main-image']}>
      <img src={mealsImage} />
    </div>
    </>
  )
}

export default Header;