import classes from './Cart.module.css';
import Modal from '../UI/Modal';
import {useContext , useState} from 'react';
import CartContext from '../../store/CartContext';
import CartItem from './CartItem';
import CheckoutForm from './CheckoutForm';

function Cart(props){
  const [userAddress , setUserAddress] = useState(false);
  const cardCtx = useContext(CartContext);
  const totalAmount = cardCtx.totalAmount;

  function cartItemRemoveHandler(id){
    cardCtx.removeItem(id);
  }

  function cartItemAddHandler(item){
    cardCtx.addItem(item)
  }
  function showUserAddress(){
    setUserAddress(true)
  }
  //sending user data and cart item to backend
  async function submitUserOrder(userDetails){
    await fetch('http://127.0.0.1:3000/friends',{
      method: 'POST',
      body: JSON.stringify({
        user: userDetails,
        orderedItems: cardCtx.items
      })
    });
  }

  let orderPlaced;
  if(userAddress){
    console.log(userAddress)
    orderPlaced = <CheckoutForm onCancel={props.hideCartHandler} submitUserOrder={submitUserOrder} />
  }
  else{
    orderPlaced =  <div className={classes.actions}>
                      <button className={classes['button--alt']} onClick={props.hideCartHandler}>Close</button>
                      <button className={classes.button} onClick={showUserAddress}>Order</button>
                   </div>
  }


  return (
    <Modal hideCartHandler={props.hideCartHandler}>
      {
        <ul className={classes['cart-items']}>
           {cardCtx.items.map( (ele) => 
              <CartItem key={ele.id} name={ele.name} amount={ele.amount} price={ele.price} onRemove={cartItemRemoveHandler.bind(null , ele.id)} onAdd={cartItemAddHandler.bind(null , ele)} />
           )}
        </ul>
      }
      <div className={classes.total}>
        <span>Total Amount</span>
        <span>{totalAmount}</span>
      </div>
      {orderPlaced}
    </Modal>
  )
}

export default Cart;