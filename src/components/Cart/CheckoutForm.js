import classes from './CheckoutForm.module.css';
import { useRef , useState} from 'react';

const Checkout = (props) => {
  const nameRef = useRef();
  const streetRef = useRef();
  const postalRef = useRef();
  const cityRef = useRef();
  const [isInputsValid , setIsInputsValid] = useState({
    name: true,
    city: true,
    street: true,
    postal: true
  });
  function isEmpty(value){
    return value.trim().length !== 0;
  }

  function isFiveChar(value){
    return value.trim().length === 5;
  }

  const confirmHandler = (event) => {
    event.preventDefault();

    const nameInputValue = nameRef.current.value;
    const streetInputValue = streetRef.current.value;
    const postalInputValue = postalRef.current.value;
    const cityInputValue = cityRef.current.value;

    const isNameInputValueValid = isEmpty(nameInputValue);
    const isStreetInputValueValid = isEmpty(streetInputValue);
    const isPostalInputValueValid = isEmpty(cityInputValue);
    const isCityInputValueValid = isFiveChar(postalInputValue);

    // console.log(isNameInputValueValid)
    // console.log(isStreetInputValueValid)
    // console.log(isPostalInputValueValid)
    // console.log(isCityInputValueValid)

    const isFormValid = isNameInputValueValid && isStreetInputValueValid
                        && isPostalInputValueValid && isCityInputValueValid;
    
    if(!isFormValid){
      setIsInputsValid({
        name: isNameInputValueValid,
        street: isStreetInputValueValid,
        postal: isPostalInputValueValid,
        city: isCityInputValueValid
      });
      return;
    }                   

    props.submitUserOrder({
        name: nameInputValue,
        street: streetInputValue,
        postal: postalInputValue,
        city: cityInputValue
    })
  };


  const nameInputClasses = `${classes.control} ${isInputsValid.name ? '' : classes.invalid}`
  const streetInputClasses = `${classes.control} ${isInputsValid.street ? '' : classes.invalid}`
  const postalInputClasses = `${classes.control} ${isInputsValid.postal ? '' : classes.invalid}`
  const cityInputClasses = `${classes.control} ${isInputsValid.city ? '' : classes.invalid}`




  return (
    <form className={classes.form} onSubmit={confirmHandler}>
      <div className={nameInputClasses}>
        <label htmlFor='name'>Your Name</label>
        <input type='text' id='name' ref={nameRef} />
        {!isInputsValid.name && <p>Please enter valid name</p>}
      </div>
      <div className={streetInputClasses}>
        <label htmlFor='street'>Street</label>
        <input type='text' id='street' ref={streetRef} />
        {!isInputsValid.street && <p>Please enter valid street</p>}
      </div>
      <div className={postalInputClasses}>
        <label htmlFor='postal'>Postal Code</label>
        <input type='text' id='postal' ref={postalRef} />
        {!isInputsValid.postal && <p>Please enter valid postal</p>}
      </div>
      <div className={cityInputClasses}>
        <label htmlFor='city'>City</label>
        <input type='text' id='city' ref={cityRef} />
        {!isInputsValid.city && <p>Please enter valid city</p>}
      </div>
      <div className={classes.actions}>
        <button type='button' onClick={props.onCancel}>
          Cancel
        </button>
        <button className={classes.submit}>Confirm</button>
      </div>
    </form>
  );
};

export default Checkout;