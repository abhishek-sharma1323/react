import classes from './AvailableMeals.module.css';
import MealItem from './MealItem';
import Card from '../UI/Card';
import { useEffect , useState } from 'react';

let items = [
  {
    id: 'm1',
    name: 'Sushi',
    description: 'Finest fish and veggies',
    price: 22.99,
  },
  {
    id: 'm2',
    name: 'Schnitzel',
    description: 'A german specialty!',
    price: 16.5,
  },
  {
    id: 'm3',
    name: 'Barbecue Burger',
    description: 'American, raw, meaty',
    price: 12.99,
  },
  {
    id: 'm4',
    name: 'Green Bowl',
    description: 'Healthy...and green...',
    price: 18.99,
  },
];


function AvailableMeals(){
  // const [items , setItems] = useState([]);
  //fetching data from my locally built API
  // useEffect(() => {
  //   fetch('http://127.0.0.1:3000/friends').then( (res) => { 
  //     return res.json()
  //   }).then( (data) => {
  //     const newData = data.map( (item) => { return {
  //       id: item.id,
  //       name: item.meal,
  //       description: "Great Taste",
  //       price: +item.price
  //     }} )
  //     setItems(newData);
  //   })
  // },[]);
  
  const meals = items.map( (ele) => (
   <MealItem key={ele.id} id={ele.id} name={ele.name} description={ele.description} price={ele.price} />
    ))
 

  return (
    <section className={classes.meals}>
    <Card>
      <ul>{meals}</ul>
    </Card>
    {/* <button onClick={loadData} >Load Data</button> */}
    </section>
  )
}

export default AvailableMeals;