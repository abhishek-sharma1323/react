import classes from './MealItem.module.css';
import MealItemForm from './MealItemForm';
import {useContext} from 'react';
import CartContext from '../../store/CartContext';
function MealItem(props){
  const cartCtx = useContext(CartContext);
  const price = `$${props.price.toFixed(2)}`;
  
  function onAddCart(value){
    console.log(value)
    cartCtx.addItem({
      id: props.id,
      price: props.price,
      name: props.name,
      amount: value, 
    });
  }

  return (
  <li className={classes.meal}>
    <div>  
      <h3>{props.name}</h3>
      <div className={classes.description}>{props.description}</div>
      <div className={classes.price}>{price}</div>
    </div> 

    <div> 
        <MealItemForm id={props.id} onAddCart={onAddCart} />
    </div>
   </li>
  )
}

export default MealItem;