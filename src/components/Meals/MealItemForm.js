import classes from './MealItemForm.module.css';
import Input from '../UI/Input';
import {useRef , useState} from 'react';

function MealItemForm(props){
  const amountInputRef = useRef();
  const [isFormValid,setIsFormValid] = useState(true);

  function onSubmitFormHandler(event){
    event.preventDefault();
    let amount = amountInputRef.current.value;
    const amountNumber = +amount;

    //validations before adding element to the cart
    if(amount.trim().length === 0 || amount > 5 || amount < 1){
      setIsFormValid(false);
      return;
    }
    props.onAddCart(amountNumber)
    // console.log(amountInputRef.current)
  }

  return (
    <form className={classes.form} onSubmit={onSubmitFormHandler}>
      <Input label="Amount" 
        ref={amountInputRef}
        input={{
          type: 'number' ,
          id: 'amount' + props.id,
          min: '1',
          max: '5',
          step: '1',
          defaultValue: '1',
        }}
      />
      <button>+ Add</button>
    { !isFormValid && <p>Please enter value (1-5)</p>}

    </form>
  )
}
export default MealItemForm;