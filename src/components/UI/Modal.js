import classes from './Modal.module.css';
import ReactDom from 'react-dom';

function Backdrop(props){
  return (
    <div className={classes.backdrop} onClick={props.hideCartHandler}/>
  )
}

function Overlay(props){
  return (
    <div className={classes.modal}>
        <div className={classes.content}>{props.children}</div>  
    </div>
  )
}

const overlayElement = document.getElementById('overlay');

function Modal(props){

  return (
    <>
    {ReactDom.createPortal(<Backdrop hideCartHandler={props.hideCartHandler} /> , overlayElement)}
    {ReactDom.createPortal(<Overlay>{props.children}</Overlay> , overlayElement)}
    </>
  )
}

export default Modal;