import Cart from "./components/Cart/Cart";
import Header from "./components/Layouts/Header";
import Meals from "./components/Meals/Meals";
import { useState } from 'react';
import CartProvider from "./store/CartProvider";

function App() {

  const [cartIsShow , setCartIsShow] = useState(false);

  function showCartHandler(){
    setCartIsShow(true)
  }

  function hideCartHandler(){
    setCartIsShow(false)
  }

  return (
    <CartProvider>
      {cartIsShow && <Cart hideCartHandler={hideCartHandler} />}
      <Header showCartHandler={showCartHandler} />
      <main> 
        <Meals  />  
      </main>
    </CartProvider>
  );
}

export default App;
