import CartContext from "./CartContext";
import {useReducer } from 'react';

const defaultValue = {
  items: [],
  totalAmount: 0,
}

function cartReducer(state , action){
  if(action.type === "ADD"){
    // const updatedItems = state.items.concat(action.payload.item);
    const updatedTotalAmount = state.totalAmount + action.payload.item.price * action.payload.item.amount;
    
    const existingCartItemIndex = state.items.findIndex((item) => item.id === action.payload.item.id)

    const existingCartItem = state.items[existingCartItemIndex]
    let updatedItem;
    let updatedItems;

    if(existingCartItem){
      updatedItem = {
        ...existingCartItem,
        amount: existingCartItem.amount + 1
      }
      updatedItems = [...state.items]
      updatedItems[existingCartItemIndex] = updatedItem;
    }
    else{

      updatedItems = state.items.concat(action.payload.item)
    }
    return {
      items: updatedItems,
      totalAmount: updatedTotalAmount,
    }
  }
  if(action.type === "REMOVE"){
    const index = state.items.findIndex( (ele) => ele.id === action.payload.id)
    const existingItem = state.items[index];
    const updatedTotalAmount = state.totalAmount  - existingItem.price;
    let updatedItems;

    if(existingItem.amount === 1){
      updatedItems = state.items.filter(item => item.id !== action.payload.id);
      const updatedTotalAmount = state.totalAmount  - existingItem.price;
    }
    else{
      const updatedItem = {...existingItem , amount: existingItem.amount - 1};
      updatedItems = [...state.items];
      updatedItems[index] = updatedItem;
    }
    return {
      items: updatedItems,
      totalAmount: updatedTotalAmount,
    }
  }
  return defaultValue;
}

function CartProvider(props){

  const [cartState , cartDispatch] = useReducer(cartReducer , defaultValue);

  function addItemToCartHandler(item){
    // console.log(item)
    cartDispatch({type: "ADD" , payload: {item: item}})
  } 
  function removeItemToCartHandler(id){
    cartDispatch({type: "REMOVE" , payload: {id: id}})
  } 

  const cartContext = {
    items: cartState.items,
    totalAmount: cartState.totalAmount,
    addItem: addItemToCartHandler,
    removeItem: removeItemToCartHandler,
  }

  return (
    <CartContext.Provider value={cartContext} >
      {props.children}
    </CartContext.Provider>
  )
}


export default CartProvider;